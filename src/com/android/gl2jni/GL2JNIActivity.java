/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gl2jni;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class GL2JNIActivity extends Activity {
    public static String EXTRA = "EXTRA";
    private GL2JNIView mView = null;
    private String msg = "";

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.main_view);
        
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set background color
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bgcolor));
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
        mView = null;
    	if(msg == null || msg == "")
            ((TextView)findViewById(R.id.text)).setText("Nothing been choosed");
        else
            ((TextView)findViewById(R.id.text)).setText(msg);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == 0) {
            if(data == null) return;
            msg = data.getStringExtra(GL2JNIExplorer.EXTRA_MESSAGE);
        }
        else
            System.out.println("Error");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mView != null)
            mView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mView != null)
            mView.onResume();
    }

    /** Called when the user clicks the send button*/
    public void sendMessage(View view){
        Intent intent = new Intent(this, GL2JNIExplorer.class);
        startActivityForResult(intent, 0);
    }

    public void startRetrace(View view) {
        if(msg != null && msg != "") {
            Intent intent = new Intent(this, RetraceView.class);
            intent.putExtra(EXTRA, msg);
            startActivity(intent);
        }
        else {
    		// Scale icon
        	Drawable scaled = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(
        			((BitmapDrawable) (getResources().getDrawable(R.drawable.awesome))).getBitmap()
        			, 50
        			, 50
        			, true));
            new AlertDialog.Builder(this)
                .setIcon(scaled)
                .setTitle("Oops")
                .setMessage("You must select a file before retracing")
                .setPositiveButton("OK",
                	new DialogInterface.OnClickListener() {
                		@Override
                		public void onClick(DialogInterface dialog, int which) {
                			// Do nothing
                		}
                	}).show();
    	}
    }
}
