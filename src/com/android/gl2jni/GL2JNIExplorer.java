package com.android.gl2jni;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GL2JNIExplorer extends ListActivity {
	/** EXTRA_MESSAGE definition */
    public final static String EXTRA_MESSAGE = "com.example.myapp.MESSAGE";
    
	private List<String> item = null;
	private List<String> path = null;
	private String root="/";
	private TextView myPath;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_gl2_jniexplorer);
        myPath = (TextView)findViewById(R.id.path);
        getDir(root);
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	Intent intent = new Intent(this, GL2JNIActivity.class);
    	intent.putExtra(EXTRA_MESSAGE, "YAY");
    }

    private void getDir(String dirPath) {
    	myPath.setText("Location: " + dirPath);
    	item = new ArrayList<String>();
    	path = new ArrayList<String>();
        File f = new File(dirPath);
        if(!dirPath.equals(root)) {
            item.add(root);
            path.add(root);
            item.add("../");
            path.add(f.getParent());
        }
        if(f.canRead()) {
	        File[] files = f.listFiles();
	        for(int i=0; i < files.length; i++) {
	            File file = files[i];
	            path.add(file.getPath());
	            if(file.isDirectory())
	                item.add(file.getName() + "/");
	            else
	                item.add(file.getName());
	        }
        }
        ArrayAdapter<String> fileList = new ArrayAdapter<String>(this, R.layout.row, item);
        setListAdapter(fileList);
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        File file = new File(path.get(position));
        if(file.isDirectory()) {
        	if(file.canRead())
        		getDir(path.get(position));
        	else {
        		// Scale icon
            	Drawable scaled = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(
            			((BitmapDrawable) (getResources().getDrawable(R.drawable.awesome))).getBitmap()
            			, 50
            			, 50
            			, true));
                new AlertDialog.Builder(this)
                    .setIcon(scaled)
                    .setTitle("Oops")
                    .setMessage("Permission denied: " + file.getPath())
                    .setPositiveButton("OK",
                    	new DialogInterface.OnClickListener() {
                    		@Override
                    		public void onClick(DialogInterface dialog, int which) {
                    			// Do nothing
                    		}
                    	}).show();
        	}
        }
        else {
        	// Scale icon
        	Drawable scaled = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(
        			((BitmapDrawable) (getResources().getDrawable(R.drawable.awesome))).getBitmap()
        			, 50
        			, 50
        			, true));
            new AlertDialog.Builder(this)
                .setIcon(scaled)
                .setTitle("Yay")
                .setMessage("You choose " + file.getPath())
                .setPositiveButton("OK", new MyListener(this, file.getPath())).show();
        }
    }

}

class MyListener implements DialogInterface.OnClickListener {
	private GL2JNIExplorer activity;
	private String message;
	
	public MyListener(GL2JNIExplorer activity, String message) {
		this.activity = activity;
		this.message = message;
	}
	
	@Override
    public void onClick(DialogInterface dialog, int which) {
		Intent intent = new Intent(activity, GL2JNIActivity.class);
		intent.putExtra(GL2JNIExplorer.EXTRA_MESSAGE, message);
		System.out.println("Message put");
		activity.setResult(0, intent);
		activity.finish();
    }
}
