package com.android.gl2jni;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class RetraceView extends Activity {
	private String fileName;
	private GL2JNIView mView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle params = getIntent().getExtras();
		if(params != null)
			fileName = params.getString(GL2JNIActivity.EXTRA);
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		mView = new GL2JNIView(getApplication(), fileName);
		
        System.out.println("set content view");
        setContentView(mView);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		mView.onPause();
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		mView.onResume();
	}
}
