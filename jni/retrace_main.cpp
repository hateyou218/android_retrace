/**************************************************************************
 *
 * Copyright 2011 rJose Fonseca
 * Copyright (C) 2013 Intel Corporation. All rights reversed.
 * Author: Shuang He <shuang.he@intel.com>
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **************************************************************************/


#include <string.h>
#include <limits.h> // for CHAR_MAX
#include <iostream>
#include <getopt.h>
#ifndef _WIN32
#include <unistd.h> // for isatty()
#endif

#include "os_binary.hpp"
#include "os_time.hpp"
#include "os_thread.hpp"
#include "retrace.hpp"
#include <android/log.h>
// TEST
#define  LOG_TAG    "retrace_main"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#include <typeinfo>

retrace::Retracer retracer;

namespace retrace {


trace::Parser parser;

int verbosity = 0;
bool debug = true;


bool doubleBuffer = true;

bool useCallNos = true;
bool singleThread = false;

unsigned frameNo = 0;
unsigned callNo = 0;


void
frameComplete(trace::Call &call) {
    ++frameNo;
}


/**
 * Retrace one call.
 *
 * Take snapshots before/after retracing (as appropriate) and dispatch it to
 * the respective handler.
 */
static void
retraceCall(trace::Call *call) {
    callNo = call->no;
    retracer.retrace(*call);
}


class RelayRunner;


/**
 * Implement multi-threading by mimicking a relay race.
 */
class RelayRace
{
private:
    /**
     * Runners indexed by the leg they run (i.e, the thread_ids from the
     * trace).
     */
    std::vector<RelayRunner*> runners;

public:
    RelayRace();

    ~RelayRace();

    RelayRunner *
    getRunner(unsigned leg);

    inline RelayRunner *
    getForeRunner() {
        return getRunner(0);
    }

    void
    run(void);

    void
    passBaton(trace::Call *call);

    void
    finishLine();

    void
    stopRunners();
};


/**
 * Each runner is a thread.
 *
 * The fore runner doesn't have its own thread, but instead uses the thread
 * where the race started.
 */
class RelayRunner
{
private:
    friend class RelayRace;

    RelayRace *race;

    unsigned leg;
    
    os::mutex mutex;
    os::condition_variable wake_cond;

    /**
     * There are protected by the mutex.
     */
    bool finished;
    trace::Call *baton;

    os::thread thread;

    static void *
    runnerThread(RelayRunner *_this);

public:
    RelayRunner(RelayRace *race, unsigned _leg) :
        race(race),
        leg(_leg),
        finished(false),
        baton(0)
    {
        /* The fore runner does not need a new thread */
        if (leg) {
            thread = os::thread(runnerThread, this);
        }
    }

    ~RelayRunner() {
        if (thread.joinable()) {
            thread.join();
        }
    }

    /**
     * Thread main loop.
     */
    void
    runRace(void) {
        os::unique_lock<os::mutex> lock(mutex);

        while (1) {
            while (!finished && !baton) {
                wake_cond.wait(lock);
            }

            if (finished) {
                break;
            }

            assert(baton);
            trace::Call *call = baton;
            baton = 0;

            runLeg(call);
        }

        if (0) LOGE("leg %d actually finishing\n", leg);

        if (leg == 0) {
            race->stopRunners();
        }
    }

    /**
     * Interpret successive calls.
     */
    void
    runLeg(trace::Call *call) {
        /* Consume successive calls for this thread. */
        do {
            static trace::ParseBookmark frameStart;

            assert(call);
            // LOGI("%s\n",call->name());
            assert(call->thread_id == leg);

            retraceCall(call);
            delete call;
            call = parser.parse_call();
        } while (call && call->thread_id == leg);

        if (call) {
            /* Pass the baton */
            assert(call->thread_id != leg);
            flushRendering();
            race->passBaton(call);
        } else {
            /* Reached the finish line */
            if (0) LOGE("finished on leg %d\n", leg);
            if (leg) {
                /* Notify the fore runner */
                race->finishLine();
            } else {
                /* We are the fore runner */
                finished = true;
            }
        }
    }

    /**
     * Called by other threads when relinquishing the baton.
     */
    void
    receiveBaton(trace::Call *call) {
        assert (call->thread_id == leg);

        mutex.lock();
        baton = call;
        mutex.unlock();

        wake_cond.signal();
    }

    /**
     * Called by the fore runner when the race is over.
     */
    void
    finishRace() {
        if (0) LOGE("notify finish to leg %d\n", leg);

        mutex.lock();
        finished = true;
        mutex.unlock();

        wake_cond.signal();
    }
};


void *
RelayRunner::runnerThread(RelayRunner *_this) {
    _this->runRace();
    return 0;
}


RelayRace::RelayRace() {
    runners.push_back(new RelayRunner(this, 0));
}


RelayRace::~RelayRace() {
    assert(runners.size() >= 1);
    std::vector<RelayRunner*>::const_iterator it;
    for (it = runners.begin(); it != runners.end(); ++it) {
        RelayRunner* runner = *it;
        if (runner) {
            delete runner;
        }
    }
}


/**
 * Get (or instantiate) a runner for the specified leg.
 */
RelayRunner *
RelayRace::getRunner(unsigned leg) {
    RelayRunner *runner;

    if (leg >= runners.size()) {
        runners.resize(leg + 1);
        runner = 0;
    } else {
        runner = runners[leg];
    }
    if (!runner) {
        runner = new RelayRunner(this, leg);
        runners[leg] = runner;
    }
    return runner;
}


/**
 * Start the race.
 */
void
RelayRace::run(void) {
    trace::Call *call;
    call = parser.parse_call();
    if (!call) {
        /* Nothing to do */
        return;
    }

    RelayRunner *foreRunner = getForeRunner();
    if (call->thread_id == 0) {
        /* We are the forerunner thread, so no need to pass baton */
        foreRunner->baton = call;
    } else {
        passBaton(call);
    }

    /* Start the forerunner thread */
    foreRunner->runRace();
}


/**
 * Pass the baton (i.e., the call) to the appropriate thread.
 */
void
RelayRace::passBaton(trace::Call *call) {
    if (0) LOGE("switching to thread %d\n", call->thread_id);
    RelayRunner *runner = getRunner(call->thread_id);
    runner->receiveBaton(call);
}


/**
 * Called when a runner other than the forerunner reaches the finish line.
 *
 * Only the fore runner can finish the race, so inform him that the race is
 * finished.
 */
void
RelayRace::finishLine(void) {
    RelayRunner *foreRunner = getForeRunner();
    foreRunner->finishRace();
}


/**
 * Called by the fore runner after finish line to stop all other runners.
 */
void
RelayRace::stopRunners(void) {
    std::vector<RelayRunner*>::const_iterator it;
    for (it = runners.begin() + 1; it != runners.end(); ++it) {
        RelayRunner* runner = *it;
        if (runner) {
            runner->finishRace();
        }
    }
}


static bool
mainLoop(int callNum) {
	// LOGI("In the mainLoop\n");
    addCallbacks(retracer);

    // bool ret = false;
    // long long startTime = 0; 
    frameNo = 0;

    // startTime = os::getTime();

    // if (singleThread) {
        trace::Call *call;
        int count = 0;
        while ((call = parser.parse_call())) {
            if(!call) return false;
            if(call->no == 0)
                count++;
            if(count == callNum)
                break;
        }
        retraceCall(call);
        while ((call = parser.parse_call())) {
            if(!call || call->no == 0) return true;
            // else LOGI("%s\n", call->name());
            /*
            std::cout << call->name() << ": ";
            for(unsigned i = 0; i < call->sig->num_args; ++i) {
                std::cout << call->sig->arg_names[i] << " = ";
                if(typeid(*(call->args[i].value)) == typeid(trace::Null))
                    std::cout << "NULL, ";
                else if(typeid(*((call->args[i]).value)) == typeid(trace::Bool))
                    std::cout << (call->args[i]).value->toBool() << ", ";
                else if(typeid(*((call->args[i]).value)) == typeid(trace::SInt))
                    std::cout << (call->args[i]).value->toSInt() << ", ";
                else if(typeid(*((call->args[i]).value)) == typeid(trace::UInt))
                    std::cout << (call->args[i]).value->toUInt() << ", ";
                else if(typeid(*((call->args[i]).value)) == typeid(trace::Float))
                    std::cout << (call->args[i]).value->toFloat() << ", ";
                else if(typeid(*((call->args[i]).value)) == typeid(trace::Double))
                    std::cout << (call->args[i]).value->toDouble() << ", ";
                else
                    std::cout << "lol, ";
            }
            std::cout << std::endl;
            */
            // if(strncmp(call->name(), "egl", 3) != 0 || strcmp(call->name(), "eglSwapBuffers") == 0)
            retraceCall(call);
            delete call;
        };
        flushRendering();
    // } else {
    //     RelayRace race;
    //     race.run();
    // }

    // long long endTime = os::getTime();
    // float timeInterval = (endTime - startTime) * (1.0 / os::timeFrequency);

    // if (retrace::verbosity >= -1)
    //     LOGI("Rendered %d frames in %f secs, average of %f fps\n", frameNo, timeInterval, frameNo/timeInterval);
    return true;
}

static void
mainLoop() {
    addCallbacks(retracer);

    long long startTime = 0; 
    frameNo = 0;

    startTime = os::getTime();

    if (singleThread) {
        trace::Call *call;
        while ((call = parser.parse_call())) {
            if(!call) return;
            retraceCall(call);
            delete call;
        };
        flushRendering();
    } else {
        RelayRace race;
        race.run();
    }

    long long endTime = os::getTime();
    float timeInterval = (endTime - startTime) * (1.0 / os::timeFrequency);

    if (retrace::verbosity >= -1)
        LOGI("Rendered %d frames in %f secs, average of %f fps\n", frameNo, timeInterval, frameNo/timeInterval);
}

bool entry(char *argv, int callNum)
{
    using namespace retrace;

    if(callNum == 1)
        retrace::setUp();
    retrace::parser.open(argv);
    bool ret = retrace::mainLoop(callNum);
    retrace::parser.close();
    return ret;
}

void entry(char *argv)
{
    using namespace retrace;

    retrace::setUp();
    retrace::parser.open(argv);
    retrace::mainLoop();
    retrace::parser.close();
}

static int width  = 480;
static int height = 800;

void setSize(int w, int h)
{
    width  = w;
    height = h;
}

int getCurrentWidth()
{
    return width;
}

int getCurrentHeight()
{
    return height;
}

} /* namespace retrace */

