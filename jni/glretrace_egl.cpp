/**************************************************************************
 *
 * Copyright 2011 LunarG, Inc.
 * All Rights Reserved.
 *
 * Based on glretrace_glx.cpp, which has
 *
 *   Copyright 2011 Jose Fonseca
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **************************************************************************/


#include "glproc.hpp"
#include "retrace.hpp"
#include "glretrace.hpp"
#include "os.hpp"
#include "eglsize.hpp"

#ifndef EGL_OPENGL_ES_API
#define EGL_OPENGL_ES_API		0x30A0
#define EGL_OPENVG_API			0x30A1
#define EGL_OPENGL_API			0x30A2
#define EGL_CONTEXT_CLIENT_VERSION	0x3098
#endif


using namespace glretrace;


typedef std::map<unsigned long long, glws::Drawable *> DrawableMap;
typedef std::map<unsigned long long, Context *> ContextMap;
typedef std::map<unsigned long long, glws::Profile> ProfileMap;
static DrawableMap drawable_map;
static ContextMap context_map;
static ProfileMap profile_map;

static unsigned int current_api = EGL_OPENGL_ES_API;
static glws::Profile last_profile = glws::PROFILE_COMPAT;

static void
createDrawable(unsigned long long orig_config, unsigned long long orig_surface);

static glws::Drawable *
getDrawable(unsigned long long surface_ptr) {
    if (surface_ptr == 0) {
        return NULL;
    }

    DrawableMap::const_iterator it;
    it = drawable_map.find(surface_ptr);
    if (it == drawable_map.end()) {
        // In Fennec we get the egl window surface from Java which isn't
        // traced, so just create a drawable if it doesn't exist in here
        createDrawable(0, surface_ptr);
        it = drawable_map.find(surface_ptr);
        assert(it != drawable_map.end());
    }

    return (it != drawable_map.end()) ? it->second : NULL;
}

static void createDrawable(unsigned long long orig_config, unsigned long long orig_surface)
{
    ProfileMap::iterator it = profile_map.find(orig_config);
    glws::Profile profile;

    // If the requested config is associated with a profile, use that
    // profile. Otherwise, assume that the last used profile is what
    // the user wants.
    if (it != profile_map.end()) {
        profile = it->second;
    } else {
        profile = last_profile;
    }

    glws::Drawable *drawable = glretrace::createDrawable(profile);
    drawable_map[orig_surface] = drawable;
}

static void retrace_eglBindAPI(trace::Call &call) {
    current_api = call.arg(0).toUInt();
    eglBindAPI(current_api);
}

static void retrace_eglSwapBuffers(trace::Call &call) {
    glws::Drawable *drawable = getDrawable(call.arg(1).toUIntPtr());

    frame_complete(call);

    if (retrace::doubleBuffer) {
        if (drawable) {
            drawable->swapBuffers();
        }
    } else {
        glFlush();
    }
}

const retrace::Entry glretrace::egl_callbacks[] = {
    {"eglGetError", &retrace::ignore},
    {"eglGetDisplay", &retrace::ignore},
    {"eglInitialize", &retrace::ignore},
    {"eglTerminate", &retrace::ignore},
    {"eglQueryString", &retrace::ignore},
    {"eglGetConfigs", &retrace::ignore},
    {"eglChooseConfig", &retrace::ignore},
    {"eglGetConfigAttrib", &retrace::ignore},
    {"eglCreateWindowSurface", &retrace::ignore},
    {"eglCreatePbufferSurface", &retrace::ignore},
    {"eglDestroySurface", &retrace::ignore},
    {"eglQuerySurface", &retrace::ignore},
    {"eglBindAPI", &retrace_eglBindAPI},
    {"eglQueryAPI", &retrace::ignore},
    {"eglSwapInterval", &retrace::ignore},
    {"eglCreateContext", &retrace::ignore},
    {"eglDestroyContext", &retrace::ignore},
    {"eglMakeCurrent", &retrace::ignore},
    {"eglGetCurrentContext", &retrace::ignore},
    {"eglGetCurrentSurface", &retrace::ignore},
    {"eglGetCurrentDisplay", &retrace::ignore},
    {"eglQueryContext", &retrace::ignore},
    {"eglWaitGL", &retrace::ignore},
    {"eglWaitNative", &retrace::ignore},
    {"eglSwapBuffers", &retrace_eglSwapBuffers},
    {"eglGetProcAddress", &retrace::ignore},
    {"eglCreateImageKHR", &retrace::ignore},
    {"eglDestroyImageKHR", &retrace::ignore},
    {"glEGLImageTargetTexture2DOES", &retrace::ignore},
    {NULL, NULL},
};
