# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := libsnappy 
LOCAL_SRC_FILES := thirdparty/libsnappy_bundled.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/KHR
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/snappy
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/zlib
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := libz 
LOCAL_SRC_FILES := thirdparty/libz_bundled.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/KHR
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/snappy
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/zlib
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := libgl2jni
LOCAL_CFLAGS    := -Werror
MY_CPP_LIST := $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_SRC_FILES := $(MY_CPP_LIST:$(LOCAL_PATH)/%=%)
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/KHR
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/snappy
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/zlib
LOCAL_LDLIBS    := -llog -lGLESv2 -lm -ldl
LOCAL_STATIC_LIBRARIES := libsnappy
LOCAL_STATIC_LIBRARIES += libz

include $(BUILD_SHARED_LIBRARY)
